# Tux Math

## A very fun game to learn tables and practice calculation.

A bunch of asteroids are falling on the city and only you can stop them. Armed with a laser cannon, you will have to correctly perform the calculations indicated on the asteroids in order to be able to aim them correctly and destroy them.

The game has many levels of difficulty, allowing you to train with additions, subtractions, multiplications, divisions and finally relative numbers. It will be perfect for schoolchildren who have to revise their tables, as well as adults who would like to challenge themselves with more difficult calculations.

This game is a rewrite for android of the famous free software TuxMath, a very popular educational software for PC.

Just like the original game, it's totally open source and free (AGPL v3 license), and without any advertising.

This new version of TuxMath brings some new features:
 * The "auto level" option: when this option is activated, the game will automatically switch to another level if the player has too much ease or too much difficulty with the operations that he must solve. 
 * Added levels with operations involving 3 numbers or more.
 * A penalty (igloo destroyed) in case of too many wrong answers (To discourage the strategy of trying all possible answers).
 * Possibility to play with 3 graphic themes: "Classic", "Original" and "Afrikalan".

## Compiling the project
These activities were created with android studio, you can compile them by
importing the project in android studio.

## Contact.

Fell free to contact us at [apps@afrikalan.org](mailto:apps@afrikalan.org).

This version of TuxMath was created as part of the Afrikalan project.

Afrikalan is a project aiming to make free educational software accessible to children in the developing countries.

This involves the creation of computers specifically designed for educational use, which are donated to public or non-profit structures receiving children: schools, libraries, helping centers, etc...

Visit us at [https://www.afrikalan.org]

You can support us at [http://afrikalan.org/mobile-soutient?o=tuxmathsource]


---


# Tux Math

## Un jeu vidéo pour apprendre ses tables et s'entrainer au calcul.

Une pluie d'astéroïdes s'abat sur la ville et vous seul pouvez l'arreter. Armé d'un canon laser, il vous faudra effectuer correctement les calculs indiqués sur les astéroïdes pour pouvoir les viser correctement et les détruire.

Le jeu dispose de nombreux niveaux de difficultés, permetant de s'entrainer avec les additions, soustractions, multiplications, divisions et enfin les nombres relatifs. Il conviendra parfaitement pour les écoliers qui doivent réviser leur tables, tout comme les adultes qui voudraient se mettre à l'épreuve avec des calculs plus difficiles.

Ce jeu est une réécriture pour android du célèbre logiciel libre TuxMath un logiciel éducatif très populaire pour PC. 

Tout comme le jeu original, il est totalement libre et gratuit (licence AGPL v3), et sans aucune publicité.

Cette nouvelle version de TuxMath apporte sont lot de nouveautés:
 * Une option "niveau auto" qui permet, si elle est activée, de basculer automatiquement vers un autre niveau si l'élève a trop de facilité ou trop de difficultés avec les opérations qu'il doit résoudre.
 * Ajout de niveaux comportant des opérations à trois nombres ou plus.
 * Un malus (igloo détruit) en cas de trop nombreuses réponses fausses (pour que la stratégie consistant à essayer toutes les réponses ne soit pas gagnante).
 * Possibilité de jouer avec 3 thèmes graphiques: "Classique", "Original" et "Afrikalan".

## Compiler le projet
Ces activités ont été créées avec android studio, vous pouvez les compiler en y
important le projet.

## Contact.

N'hésitez pas à nous contacter à [apps@afrikalan.org](mailto:apps@afrikalan.org).

Cette version de TuxMath a été créée dans le cadre du projet Afrikalan.

Afrikalan est un projet visant à rendre accessibles les logiciels libres éducatifs aux enfants dans les pays du sud.

Cela passe par la création d'ordinateurs spécifiquement conçus pour un usage éducatif, qui sont donnés à des structures publiques ou non-lucratives accueillant des enfants : écoles, bibliothèques, centres d'accueil, etc...

Visitez-nous notre site [https://www.afrikalan.org]

Vous pouvez nous soutenir sur [http://afrikalan.org/mobile-soutient?o=tuxmathsource]


---


# Tux Math

## Un videojuego para aprender tablas y practicar el cálculo.

Una lluvia de asteroides está cayendo sobre la ciudad y solo tú puedes detenerla. Armado con un cañón láser, deberás realizar correctamente los cálculos indicados en los asteroides para poder apuntarlos correctamente y destruirlos.

El juego tiene muchos niveles de dificultad, permitiéndote entrenar con sumas, restas, multiplicaciones, divisiones y finalmente números relativos. Será perfecto para escolares que tengan que repasar sus tablas, así como para adultos que quieran ponerse a prueba con cálculos más difíciles.

Este juego es una reescritura para Android del famoso software gratuito TuxMath, un software educativo muy popular para PC.

Al igual que el juego original, es totalmente de código abierto y gratuito (licencia AGPL v3), y sin ningún tipo de publicidad.

En esta nueva versión se han realizado algunas mejoras:

 * La opción "nivel automático": cuando esta opción está activada, el juego cambiará automáticamente a otro nivel si el alumno tiene demasiada facilidad o demasiada dificultad con las operaciones que debe resolver.
 * Se agregaron niveles con tres o más operaciones numéricas.
 * Una penalización (iglú destruido) en caso de demasiadas respuestas incorrectas (para que no gane la estrategia consistente en probar todas las respuestas).
 * Posibilidad de jugar con 3 temas gráficos: "Classic", "Original" y "Afrikalan". 

## Compilar el proyecto

Estas actividades fueron creadas con android studio, puede compilarlas en importando el proyecto en android studio.


## Contact.

Puede escribir nos a: [apps@afrikalan.org](mailto:apps@afrikalan.org).

Esta versión de TuxMath se creó en el marco del proyecto Afrikalan.

Afrikalan es un proyecto que tiene como objetivo hacer que el software educativo libre sea accesible para los niños de los países del sur.

Se trata de la creación de ordenadores específicamente diseñados para uso educativo, que son donados a estructuras públicas o sin ánimo de lucro que acogen a niños: colegios, bibliotecas, centros de acogida, etc...

Visita nuestra página web [https://www.afrikalan.org].

Puedes apoyar el proyecto Afrikalan aquí [http://afrikalan.org/mobile-soutient?o=tuxmathsource].










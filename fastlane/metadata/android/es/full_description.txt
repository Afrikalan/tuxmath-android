Una lluvia de asteroides está cayendo sobre la ciudad y solo tú puedes detenerla. Armado con un cañón láser, deberás realizar correctamente los cálculos indicados en los asteroides para poder apuntarlos correctamente y destruirlos.

El juego tiene muchos niveles de dificultad, permitiéndote entrenar con sumas, restas, multiplicaciones, divisiones y finalmente números relativos. Será perfecto para escolares que tengan que repasar sus tablas, así como para adultos que quieran ponerse a prueba con cálculos más difíciles.

Este juego es una reescritura para Android del famoso software gratuito TuxMath, un software educativo muy popular para PC.

Al igual que el juego original, es totalmente de código abierto y gratuito (licencia AGPL v3), y sin ningún tipo de publicidad.

En esta nueva versión se han realizado algunas mejoras:

* La opción "nivel automático": cuando esta opción está activada, el juego cambiará automáticamente a otro nivel si el alumno tiene demasiada facilidad o demasiada dificultad con las operaciones que debe resolver.
* Se agregaron niveles con tres o más operaciones numéricas.
* Una penalización (iglú destruido) en caso de demasiadas respuestas incorrectas (para que no gane la estrategia consistente en probar todas las respuestas).
* Posibilidad de jugar con 3 temas gráficos: "Classic", "Original" y "Afrikalan". 

package org.afrikalan.tuxMath.game;

import android.content.Context;
import android.content.SharedPreferences;
import android.webkit.JavascriptInterface;

import static android.content.Context.MODE_PRIVATE;


public class CookiesStoreInterface {
    SharedPreferences sharedPreferences;
    Context mContext;
    test act;

    CookiesStoreInterface(Context c,  test act) {
        this.mContext = c;
        this.act=act;
        this.sharedPreferences=this.act.getPreferences(MODE_PRIVATE);
    }

    @JavascriptInterface
    public void set(String cookieId, String val)
    {
        SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putString(cookieId, val);
        editor.commit();
    }

    @JavascriptInterface
    public String get(String cookieId) {
        return this.sharedPreferences.getString(cookieId, "");
    }

    @JavascriptInterface
    public boolean have(String cookieId) {
        return this.sharedPreferences.contains(cookieId);
    }
}



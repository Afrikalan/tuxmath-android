package org.afrikalan.tuxMath.game;

import android.content.Context;
import android.webkit.JavascriptInterface;

public class EvtHandler {
    Context mContext;
    test act;
    EvtHandler(Context c, test act) {
        this.mContext = c;
        this.act=act;
    }

    @JavascriptInterface
    public void openUrl(String url)
    {
        this.act.openUrl(url);
    }

}

package org.afrikalan.tuxMath.game;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import java.util.Locale;


public class test extends AppCompatActivity {
    private static final String TAG = test.class.getSimpleName();
    private WebView myWebView;
    String refIndex;

    boolean doubleBackToExitPressedOnce = false;
    Toast toastBackBtn;

    Handler timeOutToastBack=new Handler();

    Runnable rCancelBack = new Runnable() { public void run() {doubleBackToExitPressedOnce=false;} };

    public void goToMenu()
    {
        myWebView.post(new Runnable() {
            @Override
            public void run() {
                myWebView.loadUrl("file:///android_asset/".concat(refIndex));
            }
        });
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
    }

    public void openUrl(String url)
    {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }

    protected boolean isInGame()
    {
        String myUrl;
        myUrl=myWebView.getUrl();
        if (myUrl==null) return false;
        else return myUrl.contains("game.html");
    }

    protected void execJs(String jsCode)
    {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            myWebView.evaluateJavascript(jsCode, null);
        } else {
            myWebView.loadUrl("javascript:"+jsCode+";");
        }
    }

    @Override
    public void onBackPressed() {


        if ((! this.isInGame()) && (myWebView.canGoBack())) {
            myWebView.goBack();
            return;
        }

        if (doubleBackToExitPressedOnce) {
            doubleBackToExitPressedOnce=false;
            toastBackBtn.cancel();
            timeOutToastBack.removeCallbacks(rCancelBack);

            if (myWebView.canGoBack()) myWebView.goBack();
            else super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;

        if (myWebView.canGoBack()) toastBackBtn=Toast.makeText(this, this.getString(R.string.toastBack_menu), 2000);
        else toastBackBtn=Toast.makeText(this, this.getString(R.string.toastBack_sortir), 2000);
        toastBackBtn.show();

        timeOutToastBack.postDelayed(rCancelBack, 2000);
    }


    public Boolean amIInAfrica() {
       String Ccode=this.getUserCountry();
       String[] africanCountries={"za", "ao", "bj", "bw", "bi", "cm", "cf", "cg", "cd", "ci", "dj", "er", "et", "ga", "gm", "gh", "gn", "gw", "gq", "ke", "ls", "lr", "mg", "ml", "na", "ne", "ng", "ug", "rw", "sn", "so", "sd", "ss", "sz", "td", "tg", "zm", "zw"};
//38
       if (Ccode==null)
           return false;

       for(int i =0; i < africanCountries.length; i++)
         if(Ccode.contains(africanCountries[i]))
           return true;

       return false;
    }

    public void handleFirstRunSettings()
    {
        SharedPreferences sharedPreferences;

        sharedPreferences=this.getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (sharedPreferences.getString("opt_theme", "")=="")
        {
            if (this.amIInAfrica())
                editor.putString("opt_theme", "afrikalan");
            else
                editor.putString("opt_theme", "classic");
            editor.commit();
        }
    }

    public String getUserCountry() {
        try {
            final TelephonyManager tm = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
            final String simCountry = tm.getSimCountryIso();
            if (simCountry != null && simCountry.length() == 2) { // SIM country code is available
                return simCountry.toLowerCase(Locale.US);
            }
            else if (tm.getPhoneType() != TelephonyManager.PHONE_TYPE_CDMA) { // device is not 3G (would be unreliable)
                String networkCountry = tm.getNetworkCountryIso();
                if (networkCountry != null && networkCountry.length() == 2) { // network country code is available
                    return networkCountry.toLowerCase(Locale.US);
                }
            }
        }
        catch (Exception e) { }
        return null;
    }

    @Override
    protected void onPause()
    {
        super.onPause();

        if (this.isInGame())
            this.execJs("if (typeof objGame!='undefined') objGame.pause();");
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        if (this.isInGame())
            this.execJs("if (typeof objGame!='undefined') objGame.unPause();");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        this.handleFirstRunSettings();

        myWebView = (WebView) findViewById(R.id.monweb);
//        myWebView.setWebChromeClient(new WebChromeClient());
        if (android.os.Build.VERSION.SDK_INT >=19) myWebView.setWebContentsDebuggingEnabled(true);
        myWebView.getSettings().setAppCacheEnabled(false);
        myWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        myWebView.getSettings().setDefaultTextEncodingName("utf-8");
        if (android.os.Build.VERSION.SDK_INT >=17) myWebView.getSettings().setMediaPlaybackRequiresUserGesture(false);

        //AudioManager objAudioMan = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        //objAudioMan.setStreamVolume(AudioManager.STREAM_MUSIC, objAudioMan.getStreamMaxVolume (AudioManager.STREAM_MUSIC ), 0);


        //Adaptation des réglages selon la version d'android installée
        refIndex="index.html";


        myWebView.getSettings().setJavaScriptEnabled(true);
        EvtHandler evtHand=new EvtHandler(this, this);
        myWebView.addJavascriptInterface(evtHand, "externalEvtHandler");
        CookiesStoreInterface cookieStore=new CookiesStoreInterface(this, this);
        myWebView.addJavascriptInterface(cookieStore, "CookiesStoreInterface");


//        if (android.os.Build.VERSION.SDK_INT >= 24) {


            myWebView.setWebViewClient(new WebViewClient() {
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    // do your handling codes here, which url is the requested url
                    // probably you need to open that url rather than redirect:
                    view.loadUrl(url);
                    return false; // then it is not handled by default action
                }
            });
//        }
       this.goToMenu();


    }

}
